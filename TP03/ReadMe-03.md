** TP03 **
**Affichez l'espace disponible sur chaque nœud (machine) dans l'inventaire.**


Creation d'un playbook :

- name: Afficher l'espace disponible sur chaque nœud
  hosts: all
  tasks:
    - name: Exécuter la commande df -h
      shell: df -h
      register: df_output
    - debug:
        var: df_output.stdout_lines


- name: Afficher l'espace disponible sur chaque nœud
  hosts: all
  tasks:
    - name: Utiliser le module "debug" pour afficher l'espace disque
      debug:
        msg: "{{ ansible_hostname }} - Espace disque disponible : {{ ansible_mounts }}"



Resultat obtenue :


mika@Ansible-Master:~/mickael_ansible/TP03$ ansible-playbook -i ../hosts espace_disque.yaml

PLAY [Afficher l'espace disponible sur chaque nœud] ********************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************
ok: [10.0.5.36]
ok: [10.0.5.20]

TASK [Exécuter la commande df -h] **************************************************************************************************************************************************
changed: [10.0.5.36]
changed: [10.0.5.20]

TASK [debug] ***********************************************************************************************************************************************************************
ok: [10.0.5.20] => {
    "df_output.stdout_lines": [
        "Filesystem      Size  Used Avail Use% Mounted on",
        "udev            422M     0  422M   0% /dev",
        "tmpfs            87M  404K   86M   1% /run",
        "/dev/sdb1        30G  827M   27G   3% /",
        "tmpfs           432M     0  432M   0% /dev/shm",
        "tmpfs           5.0M     0  5.0M   0% /run/lock",
        "/dev/sdb15      124M   11M  114M   9% /boot/efi",
        "/dev/sda1       3.9G   24K  3.7G   1% /mnt",
        "tmpfs            87M     0   87M   0% /run/user/1000"
    ]
}
ok: [10.0.5.36] => {
    "df_output.stdout_lines": [
        "Filesystem      Size  Used Avail Use% Mounted on",
        "udev            422M     0  422M   0% /dev",
        "tmpfs            87M  404K   86M   1% /run",
        "/dev/sdb1        30G  827M   27G   3% /",
        "tmpfs           432M     0  432M   0% /dev/shm",
        "tmpfs           5.0M     0  5.0M   0% /run/lock",
        "/dev/sdb15      124M   11M  114M   9% /boot/efi",
        "/dev/sda1       3.9G   24K  3.7G   1% /mnt",
        "tmpfs            87M     0   87M   0% /run/user/1000"
